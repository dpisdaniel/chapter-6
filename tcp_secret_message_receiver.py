__author__ = 'Jane Doe'
"""
Author: Daniel Pakula

This is a server that receives encrypted message from people who sent the server a message using the proper client.
The server decrypts the encrypted message according to the given seq values that are sent by the client that show the
order that all the message parts should be assembled to.

*SR1 Does not work here since it checks for valid SEQ/ACK values which are trampled here due to the client sending
wrong SEQ values.
"""
import struct
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
SA_FLAGS = 'SA'
A_FLAG = 'A'
FA_FLAGS = 'FA'
START_MESSAGE = 'Decr:Go'  # Message that lets the server know that communication with a valid client is starting
END_MESSAGE = 'BYEM8'      # Message that lets the server know that the message has been fully sent
NOT_END_OF_MESSAGE = ''
FIRST_MESSAGE_PART = 0
SEQ_ELEMENT = 1
MESSAGE_ELEMENT = 0
STARTER_SEQ_VALUE = 0
FIRST_PACKET = 0


def begin_sniffing(msg_receiver, timeout=None):
    """
    Receives the msg_receiver object of MessageReceiver.
    Looks for valid clients that want our service, does the threeway handshake with them using msg_receiver
    and begins building the message using msg_receiver.
    Returns the msg_receiver with the shuffled, complete message
    optional timeout=None
    """
    syn_pak = sniff(count=1, lfilter=filter_valid_client, timeout=timeout)[FIRST_PACKET]
    while syn_pak and communicate_with_client(syn_pak, msg_receiver) != END_MESSAGE:
        syn_pak = sniff(count=1, lfilter=filter_valid_client)[FIRST_PACKET]
    return msg_receiver


def filter_valid_client(pak):
    """
    Receives a packet. Returns True if the packet is an ip, tcp and raw packet and if it starts with START_MESSAGE
    or END_MESSAGE in its RAW part.
    """
    return IP in pak and TCP in pak and Raw in pak and (str(pak[Raw]).startswith(START_MESSAGE) or str(pak[Raw]).startswith(END_MESSAGE))


def communicate_with_client(syn_pak, msg_receiver):
    """
    Receives the packet "syn_pak" with the S flag and begins communicating with the client. Appends the message
    parts to MessageReceiver "msg_receiver's" msg list.
    Also checks if this specific session is the final one. Returns a proper String that tells whether this is the
    last session or not.
    """
    msg_receiver.curr_c_pak = syn_pak
    msg_receiver.curr_client_ip = syn_pak[IP].src
    send(msg_receiver.build_sa_pak(), verbose=False)  # Sends a TCP packet with the SYN ACK flags to acknowledge the connection
    msg_receiver.curr_c_pak = sniff(count=1, lfilter=msg_receiver.filter_client_paks)[FIRST_PACKET]  # Receives the first ACK flag TCP packet from the client
    send(msg_receiver.build_a_pak(), verbose=False)
    # Receives a TCP packet with the message part in it and the SEQ value for the place of
    # the message part in the overall message.
    msg_receiver.curr_c_pak = sniff(count=1, lfilter=msg_receiver.filter_client_paks)[FIRST_PACKET]
    # Takes care of the message part.
    msg_receiver.append_data()
    # Begins closing the connection
    send(msg_receiver.build_a_pak(), verbose=False)
    msg_receiver.curr_c_pak = sniff(count=1, lfilter=msg_receiver.filter_client_paks)[FIRST_PACKET]  # Receives the first FIN flag TCP packet
    send(msg_receiver.build_fa_pak(), verbose=False)
    msg_receiver.curr_c_pak = sniff(count=1, lfilter=msg_receiver.filter_client_paks)[FIRST_PACKET]  # Receives the final packet
    if Raw in msg_receiver.curr_c_pak and str(msg_receiver.curr_c_pak[Raw].load) == END_MESSAGE:  # Checks if the final packet contains the string that lets us know that the message was fully sent
        return END_MESSAGE
    return NOT_END_OF_MESSAGE


class MessageReceiver():
    def __init__(self):
        self.msg = []
        self.curr_client_ip = ''
        self.curr_c_pak = None
        self.final_msg = ''

    @property
    def curr_c_pak(self):
        return self.curr_c_pak

    @curr_c_pak.setter
    def curr_c_pak(self, pak):
        self.curr_c_pak = pak
        self.curr_client_ip = pak[IP].src  # For convenience

    @property
    def curr_client_ip(self):
        return self.curr_client_ip

    @curr_client_ip.setter
    def curr_client_ip(self, ip):
        self.curr_client_ip = ip

    def build_sa_pak(self):
        """
        Builds a TCP packet with the SA flags
        """
        return IP(dst=self.curr_client_ip)/TCP(flags=SA_FLAGS, sport=self.curr_c_pak[TCP].dport, seq=STARTER_SEQ_VALUE, ack=self.curr_c_pak[TCP].seq + 1)

    def build_a_pak(self):
        """
        Same as build_sa_pak but with the A flag. Also checks if the client has a RAW part. If it does, adds the proper
        value to the ACK parameter.
        """
        if Raw not in self.curr_c_pak:
            return IP(dst=self.curr_client_ip)/TCP(flags=A_FLAG, sport=self.curr_c_pak[TCP].dport, seq=self.curr_c_pak[TCP].ack, ack=self.curr_c_pak[TCP].seq + 1)
        return IP(dst=self.curr_client_ip)/TCP(flags=A_FLAG, sport=self.curr_c_pak[TCP].dport, seq=self.curr_c_pak[TCP].ack, ack=self.curr_c_pak[TCP].seq + len(str(self.curr_c_pak[Raw])))

    def build_fa_pak(self):
        """
        Same as build_sa_pak but with the FA flags.
        """
        return IP(dst=self.curr_client_ip)/TCP(flags=FA_FLAGS, sport=self.curr_c_pak[TCP].dport, seq=self.curr_c_pak[TCP].ack, ack=self.curr_c_pak[TCP].seq + 1)

    def append_data(self):
        """
        Appends the message part and corresponding seq value as a tuple to self.msg
        """
        self.msg.append((str(self.curr_c_pak[Raw].load), self.curr_c_pak[TCP].seq))

    def unshuffle_message(self):
        """
        Unshuffles self.msg and puts the unshuffled message in self.final_msg. Also prints the final message
        """
        unshuffled_msg = []
        original_len = len(self.msg)
        while len(unshuffled_msg) < original_len:
            smallest_seq = self.msg[FIRST_MESSAGE_PART][SEQ_ELEMENT]  # Gives us the seq value of the first msg part in the msg list
            index = 0
            for i in range(len(self.msg)):  # Finds the smallest seq value and keeps it and its' location in index and smallest_seq
                if self.msg[i][1] < smallest_seq:
                    smallest_seq = self.msg[i][SEQ_ELEMENT]
                    index = i
            unshuffled_msg.append(self.msg[index][MESSAGE_ELEMENT])
            del self.msg[index]
        self.final_msg = "".join(unshuffled_msg)

    def filter_client_paks(self, pak):
        """
        Receives a packet. filters the packet and returns true if the packet is an ip, tcp packet and the packet's
        sender is our current client.
        """
        return IP in pak and TCP in pak and pak[IP].src == self.curr_client_ip


def main():
    msg_receiva = MessageReceiver()
    updated_msg_recver = begin_sniffing(msg_receiva)
    updated_msg_recver.unshuffle_message()

if __name__ == "__main__":
    main()