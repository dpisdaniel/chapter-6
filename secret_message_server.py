__author__ = 'John Snow'
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
FRIEND_IP = '172.16.1.88'
MESSAGE_END = '.'


def get_message():
    """
    Sniffs for the message, builds it and prints it.
    """
    pkt = sniff(count=10, lfilter=filter_packets)
    print 'packets received'
    message = ''
    index = 0
    while chr(pkt[index][UDP].dport) != MESSAGE_END:
        message += chr(pkt[index][UDP].dport)
        index += 1
    print message


def filter_packets(packet):
    """
    Filters the valid packets
    """
    return IP in packet and UDP in packet and packet[IP].src == FRIEND_IP


if __name__ == "__main__":
    get_message()