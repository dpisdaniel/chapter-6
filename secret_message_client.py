__author__ = 'John Doe'
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
SERVER_IP = '172.16.1.88'


def send_message(message):
    """
    Breaks the message to its letters and sends it to the port value of the ascii value
    """
    for letter in message:
        print ord(letter)
        packet = IP(dst=SERVER_IP)/UDP(dport=ord(letter))
        send(packet)


def main():
    msg = raw_input("Insert the message you would like to pass\n")
    send_message(msg)


if __name__ == '__main__':
    main()