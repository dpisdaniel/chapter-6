__author__ = 'Daniel'
"""
Author: Daniel Pakula

This program is a class that offers the tracerte command to the user, using icmp and the network layer.
"""
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
import time
SUCCESS_MESSAGE = 'GOOD'
FAIL_MESSAGE = 'BAD'
TTL_EXCEEDED = 11
STARTER_TTL = 1
IP_ELEMENT = 0
TIME_TOOK_ELEMENT = 1
SR1_TIMEOUT = 0.8


class Traceroute():
    def __init__(self, dst, hops=None, show=True):
        self.dst = dst
        self.hops = hops
        self.ttl = STARTER_TTL
        self.router_list = []
        self.show = show

    def _measure_hop(self):
        """
        Finds the ip of the router for the current ttl value on the way to the destination
        """
        pak = IP(dst=self.dst, ttl=self.ttl) / ICMP(type='echo-request')
        before_time = time.time()
        ret_pak = sr1(pak, timeout=SR1_TIMEOUT, verbose=False)
        time_took = time.time() - before_time
        if ret_pak and ret_pak[ICMP].type == TTL_EXCEEDED:
            self.router_list.append((ret_pak[IP].src, time_took))
            self.ttl += 1
            return SUCCESS_MESSAGE
        return FAIL_MESSAGE

    def trace_route(self):
        """
        Finds the entire route to the destination and prints the route if the self.show parameter is True
        """
        succeeded = self._measure_hop()
        while succeeded == SUCCESS_MESSAGE:
            succeeded = self._measure_hop()
        if self.show:
            self._print_routers()

    def _print_routers(self):
        """
        Prints all the router ips on the way to the destination
        """
        if self.hops:  # Checks if the user wanted to print just the first self.hops hops
            if self.hops > len(self.router_list):
                self.hops = len(self.router_list)
            for i in range(self.hops):
                print self.router_list[i][IP_ELEMENT] + ' Time Took: ' + str(self.router_list[i][TIME_TOOK_ELEMENT])
        else:  # This happens if the user didnt want any specific number of hops to be printed
            for i in range(len(self.router_list)):
                print self.router_list[i][IP_ELEMENT] + 'Time Took: ' + str(self.router_list[i][TIME_TOOK_ELEMENT])


def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        raise SyntaxError
    dst = sys.argv[1]
    hops = None
    if len(sys.argv) >= 3:
        hops = int(sys.argv[2])
    tracer = Traceroute(dst, hops=hops)
    tracer.trace_route()

if __name__ == "__main__":
    main()