__author__ = 'Daniel'
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
RANDOM_SEQ = 123  # (not really random)
SA_FLAGS = 18
S_FLAG = 'S'


def check_ports(ip):
    """
    RECEIVES THE IP AND CHECKS WHICH PORTS ARE OPEN IN THE RANGE OF 20 - 1025 NOT INCLUDING 1025
    """
    ports = range(20, 1025)
    opened_ports = []
    for port in ports:
        print port
        my_syn = TCP(flags=S_FLAG, seq=RANDOM_SEQ, dport=port)
        pak = IP(dst=ip)/my_syn
        response_pak = sr1(pak, timeout=1)
        if response_pak and response_pak[TCP].flags == SA_FLAGS:
            opened_ports.append(port)
    for port in opened_ports:
        print port


def main():
    ip = raw_input("Insert the ip that u'd like to see the open ports of\n")
    while not validate_ip(ip):
        ip = raw_input("YOUR IP WAS NOT CORRRECT!!@#!$!@$ INSERT A NEW ONE\n")
    check_ports(ip)


def validate_ip(s):
    """
    Receives a string and checks if it's in an ip format.
    """
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

if __name__ == "__main__":
    main()
