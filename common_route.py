__author__ = 'Daniel'
"""
Author: Daniel Pakula.

This program receives 2 destinations from the user, finds the routers on the way to the destinations for each
destination and shows the comman route of the two destinations.
"""
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
import tracerout
IP_ELEMENT = 0


def show_common_route(router_l1, router_l2):
    """
    Receives two lists of tuples of router ips and the time that it took for the router to respond and prints the
    common route from the start for the two lists.
    """
    index = 0
    while index < len(router_l2) and index < len(router_l1) and router_l1[index][IP_ELEMENT] == router_l2[index][IP_ELEMENT]:
        print router_l1[index][IP_ELEMENT]
        index += 1

if __name__ == "__main__":
    if len(sys.argv) < 3 or len(sys.argv) > 3:
        raise SyntaxError
    dst1 = sys.argv[1]
    dst2 = sys.argv[2]
    trace1 = tracerout.Traceroute(dst1, show=False)
    trace1.trace_route()
    trace2 = tracerout.Traceroute(dst2, show=False)
    trace2.trace_route()
    show_common_route(trace1.router_list, trace2.router_list)