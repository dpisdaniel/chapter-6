__author__ = 'John Doe'
"""
Author: Daniel Pakula

This is a client that receives a message and sends it to the given server encrypted. The message is split to
parts and is sent in a random order. The place of the message part in the overall message is sent to the server
as the SEQ value. The client does the threeway handshake with the server and then sends the message part over, and then
closes the connection. This repeats for every message part.

*SR1 Does not work here since it checks for valid SEQ/ACK values which are trampled here due to the program sending
wrong SEQ values on purpose..
"""
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
import sys
istd, ostd , estd = sys.stdin, sys.stdout, sys.stderr
from scapy.all import *
from scapy.layers.dns import DNS, DNSQR, DNSRR
sys.stdin, sys.stdout, sys.stderr = istd, ostd, estd
from random import randint, choice, shuffle
from time import sleep
S_FLAG = 'S'
ACK_FLAG = 'A'
F_FLAG = 'F'
FA_FLAGS = 'FA'
SA_FLAGS = 'SA'
RA_FLAGS = 'RA'
R_FLAG = 'R'
START_MESSAGE = 'Decr:Go'  # Message that lets the server know that communication with a valid client is starting
END_MESSAGE = 'BYEM8'      # Message that lets the server know that the message has been fully sent
AVAILABLE_PORTS_TO_SEND_TO = range(15000, 50000)
DEFAULT_DPORT = 80
DEFAULT_SEQ_VALUE = 0
DEFAULT_ACK_VALUE = 0
DEFAULT_TCP_FLAG = 0
DEFAULT_IP_DST = '127.0.0.1'
SLEEP_TIME = 1  # This can be reduced from 1 a bit.
FINAL_MESSAGE_ELEMENT = -1
MESSAGE_ELEMENT = 0
SEQ_VALUE_ELEMENT = 1
FIRST_PACKET = 0


def build_packet(tcp_flags=DEFAULT_TCP_FLAG, ack=DEFAULT_ACK_VALUE, seq=DEFAULT_SEQ_VALUE, dport=DEFAULT_DPORT, dst=DEFAULT_IP_DST, sport=None):
        """
        Receives the tcp flags, seq value, ack value, destination port, destination ip and optional source port,
        and builds a scapy packet accordingly
        """
        if not sport:
            return IP(dst=dst) / TCP(flags=tcp_flags, seq=seq, dport=dport, ack=ack)
        return IP(dst=dst) / TCP(flags=tcp_flags, seq=seq,sport=sport, dport=dport, ack=ack)


class MessageSender():
    def __init__(self, message, parts, dst_ip):
        if len(message) < parts or parts == 0:
            raise ValueError
        self.parts = parts
        self.dst_ip = dst_ip
        self.message = message
        self.split_msg_tuple = self.split_message()

    def send_msg_to_server(self):
        """
        Sends the split message to the server in the wrong order.
        """
        split_msg_tuple_backup = self.split_msg_tuple
        shuffle(split_msg_tuple_backup)  # Splits our message order so the messaged will be sent in the wrong order
        for index in range(len(split_msg_tuple_backup) - 1):
            self.lets_handshake(split_msg_tuple_backup[index])
            sleep(SLEEP_TIME)
        sleep(SLEEP_TIME)
        self.lets_handshake(split_msg_tuple_backup[FINAL_MESSAGE_ELEMENT], last_message=True)  # Sends the last message part

    def lets_handshake(self, message_part, last_message=False):
        """
        Receives the message part that needs to be sent as a tuple of the msg and correct seq value,
        sends it using a threeway handshake to open the connection and then closes the connection.
        Returns True if it successfully sent the message and closed the connection and false otherwise.
        """
        server_port = choice(AVAILABLE_PORTS_TO_SEND_TO)  # Chooses a random port to send to
        ret_ack_pak = self.three_way_hand_shake(server_port)
        if ret_ack_pak:
            self.send_message_part(server_port, message_part, ret_ack_pak)
            ret_ack_pak = sniff(count=1, lfilter=self.filter_client_pak)[FIRST_PACKET]  # waits for the server to confirm it getting the message packet
            return self.close_connection(ret_ack_pak, server_port, final_part=last_message)

    def three_way_hand_shake(self, dport):
        """
        Receives the server destination port and does the threeway handshake with the server. Returns the ACK packet
        from the server after the threeway handshake is done. Returns None if communication with the server failed.
        """
        my_syn_packet = build_packet(tcp_flags=S_FLAG, dport=dport, dst=self.dst_ip)
        pak = my_syn_packet/Raw(START_MESSAGE)  # Loads the packet with the start message to let the server know we are one of its clients
        send(pak, verbose=False)
        return_pak = sniff(count=1, lfilter=self.filter_client_pak)[FIRST_PACKET]  # Receives the SYN ACK packet from the server
        if return_pak:
            pak = build_packet(tcp_flags=ACK_FLAG, seq=return_pak[TCP].ack, ack=return_pak[TCP].seq + 1, dport=dport, dst=self.dst_ip)
            send(pak, verbose=False)  # Sends the ACK packet to the server as part of the handshake, thus ending the handshake
            ret_ack_pak = sniff(count=1, lfilter=self.filter_client_pak)[FIRST_PACKET]
            return ret_ack_pak
        return None

    def send_message_part(self, dport, message_part, last_server_packet):
        """
        Receives the destination port and message part. message_part is a tuple built of the message part string
        and seq ack to send the message with like this (message, seq)
        """
        pak = build_packet(tcp_flags=ACK_FLAG, seq=message_part[SEQ_VALUE_ELEMENT], ack=last_server_packet[TCP].seq + 1, dport=dport, dst=self.dst_ip)
        pak = pak/Raw(message_part[MESSAGE_ELEMENT])  # Loads the message to the raw part of the packet
        send(pak, verbose=False)

    def close_connection(self, last_server_packet, dport, final_part=False):
        """
        Closes the connection with the server. Sends FIN, receives FIN ACK and then sends ACK and ends the connection.
        Also checks if this was the final part of the message. If it is, loads the final ACK packet with the END_MESSAGE
        string to let the server know that the conversation is ending.
        """
        # Sends the FIN flag packet
        pak = build_packet(dst=self.dst_ip, tcp_flags=F_FLAG, seq=last_server_packet[TCP].ack, ack=last_server_packet[TCP].seq + 1, dport=dport)
        send(pak, verbose=False)
        # Receives the FIN ACK packet from the server
        return_pak = sniff(count=1, lfilter=self.filter_client_pak)[FIRST_PACKET]
        if return_pak:
            # Builds the final ACK packet for the server
            my_fin = build_packet(dst=self.dst_ip, tcp_flags=ACK_FLAG, seq=return_pak[TCP].ack, ack=return_pak[TCP].seq + 1, dport=dport)
            if not final_part:
                pak = my_fin
            else:
                pak = my_fin/Raw(END_MESSAGE)
            send(pak, verbose=False)
            return True
        return False

    def split_message(self):
        """
        Splits our message to self.parts parts with random lengths for each part and randomizes the order
        of the messages
        """
        split_message = []
        index = 0
        while len(split_message) < self.parts - 1:
            available_max_split = len(self.message[index:]) - (self.parts - len(split_message) - 1)
            # Got the maximum length that can be in one part of the message for this part of the message
            random_length = randint(1, available_max_split)
            # Got the actual length that the message will be split to
            split_message.append(self.message[index: index + random_length])
            index += random_length
        split_message.append(self.message[index:])
        split_message = self.arrange_split_msg(split_message)
        return split_message

    def arrange_split_msg(self, split_message):
        """
        Receives the split message list and returns a new list of the split message and corresponding seq value
        as a tuple for each of the new list's elements.
        """
        msg_index = 0
        msg_tuple_list = []
        for msg in split_message:  # Creates the seq value for each part of the msg after the msg part in the list
            msg_tuple_list.append((msg, msg_index))
            msg_index += len(msg)
        return msg_tuple_list

    def filter_client_pak(self, pak):
        """
        Receives the packet and return True if the packet is an ip and tcp packet and the packet's sender is the server
        and the packet is not a RST flag TCP packet or RST/ACK TCP packet.
        """
        return IP in pak and TCP in pak and pak[IP].src == self.dst_ip and pak[TCP].flags != RA_FLAGS and pak[TCP].flags != R_FLAG


def main():
    msg_sender = MessageSender('abcd', 0, '172.16.1.100')
    msg_sender.send_msg_to_server()

if __name__ == "__main__":
    main()
