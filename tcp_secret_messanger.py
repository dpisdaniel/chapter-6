__author__ = 'Daniel'
"""
Author: Daniel Pakula

This program uses the tcp_secret_message receiver and sender to create a chat between to clients. Each client
can send and receive messages to one another.
"""
import tcp_secret_message_receiver
import tcp_secret_message_sender
import threading
from time import strftime
from random import randint
EXIT = 'EXIT'
DSTIP = '172.16.1.139'


def receive_messages():
    """
    Runs in the background and attempts to receive messages from whoever sends them to us
    """
    while True:
        msg_receiver = tcp_secret_message_receiver.MessageReceiver()
        updated_msg_recver = tcp_secret_message_receiver.begin_sniffing(msg_receiver)
        updated_msg_recver.unshuffle_message()
        print "Received at %s from %s ~ %s\n" % (strftime("%H:%M:%S"), msg_receiver.curr_client_ip, msg_receiver.final_msg)

if __name__ == "__main__":
    thread = threading.Thread(target=receive_messages)
    thread.daemon = True
    thread.start()
    while True:
        message = raw_input("Enter the message you would like to send, enter EXIT to leave\n")
        if message == EXIT:
            break
        parts = randint(1, len(message))  # amount of parts to split the message to
        msg_sender = tcp_secret_message_sender.MessageSender(message, parts, DSTIP)
        msg_sender.send_msg_to_server()
        print "Sent message successfully at %s\n" % strftime("%H:%M:%S")